from django.urls import path, include
from . import views


urlpatterns = [
    path('', views.index),
    path('alimentadores', views.alimentadores),
    path('alimentadores/add', views.add_content),
    path('alimentadores/<str:feeder>', views.pagina_alimentador),
    path('alimentadores/<str:feeder>/<str:id>', views.pagina_item),
    path('alimentadores/<str:feeder>/<str:id>/vote', views.vote),
    path('usuarios', views.usuarios),
    path('usuarios/<str:username>', views.usuario),
    path('usuarios/<str:username>/image', views.changeImage),
    path('informacion', views.informacion),
    path('loggedIn', views.loggedIn),
    path('logout', views.logoutView),
    path('register', views.register),
    path('i18n/', include('django.conf.urls.i18n')),
]