from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

class Alimentador(models.Model):
    id = models.CharField(max_length=64, primary_key=True)
    titulo = models.CharField(max_length=64, null=True, blank=False)
    link = models.CharField(max_length=64, null=True, blank=False)
    num_items = models.IntegerField(default=0)
    puntuacion = models.IntegerField(default=0)
    elegido = models.BooleanField(default=True)

    def __str__(self):
        return self.titulo

    def es_wikipedia(self):
        return ('Revision history' in self.titulo)

class Item(models.Model):
    alimentador = models.ForeignKey(Alimentador, on_delete=models.CASCADE, null=True)
    titulo = models.CharField(max_length=64, null=True)
    link = models.CharField(max_length=64, null=True)
    id = models.CharField(max_length=64, primary_key=True)
    descripcion = models.CharField(max_length=1024, null=True)
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    puntuacion = models.IntegerField(default=0)

    def __str__(self):
        return self.titulo

class Voto(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    voto = models.IntegerField(default=0)

    def __str__(self):
        if self.voto < 0:
            return self.user.username + " ha dado dislike"
        else:
            return self.user.username + " ha dado like"

class Comentario(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=64, null=True)
    comentario = models.CharField(max_length=256, null=True)
    fecha = models.DateTimeField(default=datetime.now, blank=True)
    imagen = models.ImageField(blank=True, null=True, upload_to="miscosas")

    def __str__(self):
        return self.user.username + " comenta " + self.comentario + " | " + self.item.titulo

class Imagen(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null = True)
    imagen = models.ImageField(default="default.png", upload_to="miscosas")

    def __str__(self):
        return self.imagen.name

class Opciones(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    MODO = (('L', 'Ligero'), ('O', 'Oscuro'),)
    modo = models.CharField(max_length=1, choices=MODO, default='Ligero',
            help_text='Modo de color para la página')
    SIZE = (('P', 'Pequeña'), ('M', 'Mediana'),('G', 'Grande'),)
    size = models.CharField(max_length=1, choices=SIZE, default='M',
            help_text='Tamaño de la letra')

    def __str__(self):
        return self.user.username + " " + self.modo + " " + self.size

