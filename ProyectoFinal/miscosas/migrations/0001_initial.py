# Generated by Django 3.0.3 on 2020-05-17 11:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Alimentador',
            fields=[
                ('id', models.CharField(max_length=64, primary_key=True, serialize=False)),
                ('titulo', models.CharField(max_length=64)),
                ('link', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('video', models.CharField(max_length=64)),
                ('titulo', models.CharField(max_length=64)),
                ('link', models.CharField(max_length=64)),
                ('id', models.CharField(max_length=64, primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=256)),
                ('likes', models.IntegerField(default=0)),
                ('dislikes', models.IntegerField(default=0)),
                ('alimentador', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='miscosas.Alimentador')),
            ],
        ),
    ]
