from django import forms
from django.contrib.auth.models import User
from .models import Alimentador, Comentario, Imagen, Opciones

class LoginForm(forms.Form):

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class ComentarioForm(forms.ModelForm):

    class Meta:
        model = Comentario
        fields = ('titulo', 'comentario', 'imagen')

class ImagenForm(forms.ModelForm):

    class Meta:
        model = Imagen
        fields = ['imagen',]