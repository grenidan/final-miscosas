#!/usr/bin/python3
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from .models import Alimentador, Item
import sys
import string

class YTHandler(ContentHandler):

    def __init__ (self):
        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.title = ""
        self.id = ""
        self.link = ""
        self.channel = ""
        self.channelName = ""
        self.channelLink = ""
        self.channelId = ""
        self.description = ""
        self.item = []

    def startElement (self, name, attrs):

        if name == 'entry':
            self.inEntry = True

        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
            elif name == 'yt:videoId':
                self.inContent = True
            elif name == 'media:thumbnail':
                self.image = attrs.get('url')
            elif name == 'media:description':
                self.inContent = True
            elif name == 'name':
                self.inContent = True
            elif name == 'uri':
                self.inContent = True
            elif name == 'yt:channelId':
                self.inContent = True

    def endElement (self, name):

        if name == 'entry':
            self.inEntry = False
            try:
                channel = Alimentador.objects.get(titulo = self.channelName)
            except:
                channel = Alimentador(id = self.channelId, titulo = self.channelName, link = self.channelLink, 
                                        num_items=len(self.item), puntuacion=0)
            
            self.channel = channel
            channel.save()

            try:
                item = Item.objects.get(id = self.id)
                self.item.append(item)
            except Item.DoesNotExist:
                item = Item(alimentador = self.channel, titulo = self.title, link = self.link, id=self.id,
                            descripcion = self.description, likes = 0, dislikes = 0)
                item.save()
                channel.num_items = channel.num_items + 1
                self.item.append(item)

        elif self.inEntry:
            if name == 'title':
                self.title = self.content
                self.content = ""
                self.inContent = False
            elif name == 'yt:videoId':
                self.id = self.content
                self.content = ""
                self.inContent = False
            elif name == 'media:description':
                self.description = self.content
                self.content = ""
                self.inContent = False
            elif name == 'uri':
                self.channelLink = self.content
                self.content = ""
                self.inContent = False
            elif name == 'name':
                self.channelName = self.content
                self.content = ""
                self.inContent = False
            elif name == 'yt:channelId':
                self.channelId = self.content
                self.content = ""
                self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class YTChannel:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = YTHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def videos (self):
        return self.handler.item
