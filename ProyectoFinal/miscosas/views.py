from django.shortcuts import render, redirect, get_object_or_404, render
from .forms import LoginForm, ComentarioForm, ImagenForm
from .models import Alimentador, Item, Voto, Comentario, Imagen, Opciones
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.template.loader import get_template
from django.template import Context, RequestContext
from .ytchannel import YTChannel
from .wikipage import WikiArticle
import urllib
import json
from django.core import serializers
from django.forms.models import model_to_dict
import xml.etree.cElementTree as ET
from django.conf import settings
from django.utils import translation
from django.utils.translation import gettext as _
from django.utils.translation import check_for_language

def set_language(request):
    language = request.POST.get('language', settings.LANGUAGE_CODE)
    cur_language = translation.get_language()
    try:
        translation.activate(language)
        text = translation.gettext('Cambie el idioma')
    finally:
        translation.activate(cur_language)
        text = translation.gettext('Cambie el idioma')
    return redirect('/')

def seleccionar(request):
    if request.method == 'POST':
        action = request.POST['Select']
        id = request.POST['id']
        if id != "":
            alimentador = get_object_or_404(Alimentador, id=id)
            if action == 'Elegir' or action == 'Select':
                alimentador.elegido = True
                if alimentador.es_wikipedia():
                    nombre = id.split()[0]
                    url = "https://en.wikipedia.org/w/index.php?title=" + nombre + "&action=history&feed=rss"
                    xmlStream = urllib.request.urlopen(url)
                    articulo = WikiArticle(xmlStream)
                    articulo = Alimentador.objects.get(titulo=id)
                    items = Item.objects.filter(alimentador=articulo)
                    alimentador.num_items = len(items)
                    alimentador.save()
                    redirect = '/alimentadores/' + str(alimentador.titulo)
                else:
                    url = "https://www.youtube.com/feeds/videos.xml?channel_id=" + id
                    xmlStream = urllib.request.urlopen(url)
                    canal = YTChannel(xmlStream)
                    canal = Alimentador.objects.get(id=id) 
                    items = Item.objects.filter(alimentador=canal)
                    alimentador.num_items = len(items)
                    alimentador.save()
                    redirect = '/alimentadores/' + str(alimentador.titulo)
            elif action == 'Eliminar' or action == 'Delete':
                alimentador.elegido = False
                alimentador.save()
                redirect = '/'
            return redirect

def barraxml(user):
    template = get_template("main.xml")
    alimentadores = Alimentador.objects.filter(elegido=True)
    items = Item.objects.order_by('-puntuacion')[:10]
    c = RequestContext(user, {"alimentadores" : alimentadores, "items" : items}).flatten()
    if user.is_authenticated:
        lastvoted = Voto.objects.filter(user=user)[:5]
        c = RequestContext(user, {"user":user,"alimentadores" : alimentadores, "items" : items, "voteditems":lastvoted}).flatten()
    respuesta = template.render(c)
    return HttpResponse(respuesta, status=200, content_type="application/xml")

def barrajson(user):
    all = [*Alimentador.objects.filter(elegido=True), *Item.objects.order_by('-puntuacion')[:10]]
    indexjson = serializers.serialize('json', all)
    if user.is_authenticated:
        all = [*Alimentador.objects.filter(elegido=True), *Item.objects.order_by('-puntuacion')[:10], *Voto.objects.filter(user=user)[:5]]
        indexjson = serializers.serialize('json', all)
    return indexjson

def index(request):

    set_language(request)
    if request.method == "GET":
        formato = request.GET.get('format')
        if formato:
            if formato == 'xml':
                return HttpResponse(barraxml(request.user), status=200, content_type="application/xml")
            else:
                return HttpResponse(barrajson(request.user), status=200, content_type="application/json")
    if seleccionar(request) != None:
        return redirect(seleccionar(request))
    canales = []
    articulos = []
    alimentadores = Alimentador.objects.filter(elegido=True)
    for alimentador in alimentadores:
        if not alimentador.es_wikipedia():
            canales.append(alimentador)
        else:
            articulos.append(alimentador)
    items = Item.objects.order_by('-puntuacion')[:10]
    context = {'alimentadores': alimentadores,'canales':canales,'articulos':articulos,'items': items}
    if request.user.is_authenticated:
        user = request.user
        itemsvotados = []
        votos = Voto.objects.filter(user=user)
        for voto in votos:
            itemsvotados.append(voto.item)
        lastvoted = votos[:5]
        imagenusuario = Imagen.objects.get(user=user)
        opciones = Opciones.objects.get(user=user)
        context = {"imagenusuario":imagenusuario, "opciones":opciones, "alimentadores":alimentadores,
                    "canales":canales, "articulos":articulos, 'items': items,'lastvoted': lastvoted, 
                    'votos': votos, 'itemsvotados':itemsvotados, 'user': user}

    return render(request, "index.html", context)

def alimentadores(request):

    if seleccionar(request) != None:
        return redirect(seleccionar(request))
    canales = []
    articulos = []
    alimentadores = Alimentador.objects.all()
    for alimentador in alimentadores:
        if not alimentador.es_wikipedia():
            canales.append(alimentador)
        else:
            articulos.append(alimentador)

    context = {"alimentadores":alimentadores,"canales":canales, "articulos":articulos}
    user = request.user
    if user.is_authenticated:
        opciones = Opciones.objects.get(user=user)
        imagenusuario = Imagen.objects.get(user=user)
        context = {"opciones":opciones, "alimentadores":alimentadores,"canales":canales, "articulos":articulos,"imagenusuario":imagenusuario} 
    return render(request, 'alimentadores.html', context)

def add_content(request):
    id = ""
    nombre_articulo = ""
    if request.method == 'POST':
        try:
            nombre_articulo = request.POST['articulo']
        except:
            id = request.POST['id']
            if id == "":
                return HttpResponse("Inserte el nombre del contenido")
        if id == "":
            titulo = str(nombre_articulo) + " - Revision history"
            url = "https://en.wikipedia.org/w/index.php?title=" + nombre_articulo + "&action=history&feed=rss"
            xmlStream = urllib.request.urlopen(url)
            articulo = WikiArticle(xmlStream)
            articulo = Alimentador.objects.get(titulo=titulo)
            items = Item.objects.filter(alimentador=articulo)
            articulo.num_items = len(items)
            articulo.elegido = True
            articulo.save()
            url = '/alimentadores/' + titulo
        elif nombre_articulo == "":
            url = "https://www.youtube.com/feeds/videos.xml?channel_id=" + id
            xmlStream = urllib.request.urlopen(url)
            canal = YTChannel(xmlStream)
            canal = Alimentador.objects.get(id=id) 
            items = Item.objects.filter(alimentador=canal)
            canal.num_items = len(items)
            canal.elegido = True
            canal.save()
            url = '/alimentadores/' + str(canal.titulo)
        return redirect(url)

def usuario(request, username):

    try:
        user = request.user
        userpage = User.objects.get(username=username)
        if user.is_authenticated:
            opciones = Opciones(user=user)
            size = Opciones
            if request.method == "POST" and user == userpage:
                action = request.POST['Ajustar']
                if action == "Mediana" or action == "Medium":
                    opciones = Opciones.objects.filter(user=user).update(size="M")

                if action == "Pequeña" or action == "Small":
                    opciones = Opciones.objects.filter(user=user).update(size="P")

                if action == "Grande" or action == "Big":
                    opciones = Opciones.objects.filter(user=user).update(size="G")

                if action == "Oscuro" or action == "Dark":
                    opciones = Opciones.objects.filter(user=user).update(modo="O")

                if action == "Ligero" or action == "Light":
                    opciones = Opciones.objects.filter(user=user).update(modo="L")
        try:
            imagen = Imagen.objects.get(user=userpage)
        except Imagen.DoesNotExist:
            pass 
        votos = Voto.objects.filter(user=userpage)
        comentarios = Comentario.objects.filter(user=userpage)

        context = {"user":user,"userpage":userpage,"votos":votos,"comentarios":comentarios,"imagen":imagen}
        
        if user.is_authenticated:
            opciones = Opciones.objects.get(user=user)
            imagenusuario = Imagen.objects.get(user=user)
            context = {"user":user, "opciones":opciones, "userpage": userpage,"votos":votos, "comentarios":comentarios, 
                        "imagen":imagen, "imagenusuario":imagenusuario}
        return render(request, "usuario.html", context)
    
    except User.DoesNotExist:
        return HttpResponse("Usuario no encontrado")

def usuarios(request):

    users = User.objects.all()
    votos = Voto.objects.all()
    comentarios = Comentario.objects.all()
    imagenes = Imagen.objects.all()
    alimentadores = Alimentador.objects.all()

    context = {"alimentadores":alimentadores,"users":users,"votos":votos,"comentarios":comentarios,"imagenes":imagenes}
    user = request.user
    if user.is_authenticated:
        opciones = Opciones.objects.get(user=user)
        imagenusuario = Imagen.objects.get(user=user)
        context = {"opciones":opciones, "alimentadores":alimentadores, "users":users, "votos":votos, 
                    "comentarios":comentarios, "imagenes":imagenes, "imagenusuario":imagenusuario}
    return render(request, 'usuarios.html', context)

def changeImage(request, username):
    user = User.objects.get(username=username)
    if request.method == 'POST' and user == request.user:
        form = ImagenForm(request.FILES)
        if form.is_valid():
            descarga = request.FILES.get("imagen")

            if descarga != None:
                vieja = Imagen.objects.get(user = user) 
                vieja.delete() 
                imagen = Imagen(user=user, imagen=descarga)
                imagen.save()
                return redirect("/usuarios/" + username)
            else:
                return HttpResponse("Introduce una imagen")
    else:
        form = ImagenForm()

    context = {"form":form, "user":user}

    user = request.user
    if user.is_authenticated:
        opciones = Opciones.objects.get(user=user)
        imagenusuario = Imagen.objects.get(user=user)
        context = {"form":form, "user":user, "opciones":opciones, "imagenusuario":imagenusuario}
    return render(request, 'upload.html', context)

def pagina_alimentador(request, feeder):

    alimentador = get_object_or_404(Alimentador, titulo=feeder)
    items = []
    alimentadores = Alimentador.objects.all()
    context = {"alimentador": alimentador, 'items': Item.objects.filter(alimentador=alimentador)}

    user = request.user
    if user.is_authenticated:
        opciones = Opciones.objects.get(user=user)
        imagenusuario = Imagen.objects.get(user=user)
        context = {"opciones":opciones, "alimentador": alimentador,'items': Item.objects.filter(alimentador=alimentador),"imagenusuario":imagenusuario}

    return render(request, "alimentador.html", context)

def pagina_item(request, feeder, id):

    url = ""
    user = request.user
    if request.method == "POST" and user.is_authenticated:
        form = ComentarioForm(request.POST, request.FILES)
        if form.is_valid():
            titulo = request.POST["titulo"]
            texto = request.POST["comentario"]
            descarga = request.FILES.get("imagen")
            item = get_object_or_404(Item, id=id)
            comentario = Comentario(user=user, item = item, titulo=titulo, comentario=texto, imagen=descarga)
            comentario.save()
    else:
        form = ComentarioForm()

    item = get_object_or_404(Item, id=id)
    if not "- Revision history" in str(item.alimentador):
        url = "https://www.youtube.com/embed/" + id
    comentarios = Comentario.objects.filter(item=item)
    context = {"item": item, "comentarios":comentarios, "url":url}

    if user.is_authenticated:
        opciones = Opciones.objects.get(user=user)
        imagenusuario = Imagen.objects.get(user=user)
        try:
            voto = Voto.objects.filter(user=user).get(item=item)
            context = {"url":url, "form":form,"opciones":opciones,"item": item,"comentarios":comentarios,"voto":voto,"imagenusuario":imagenusuario}
        except Voto.DoesNotExist:
            context = {"url":url, "form":form,"opciones":opciones, "item": item,"comentarios":comentarios,"imagenusuario":imagenusuario}
    
    return render(request, "item.html", context)

def vote(request, feeder, id):
    if request.method == "POST":
        item = get_object_or_404(Item, id=id)
        vote = request.POST['vote']
        page = request.POST['page']
        user = request.user
        try:
            oldvote = Voto.objects.filter(user=user).get(item = item)
            if oldvote.voto == 1 and vote == "dislike":
                item.likes -= 1
                item.dislikes += 1
                item.puntuacion = item.likes - item.dislikes
                item.save()
                oldvote.voto = -1
                oldvote.save()
                item.alimentador.puntuacion -= 2
                item.alimentador.save()
            if oldvote.voto == -1 and vote == "like":
                item.dislikes -= 1
                item.likes += 1
                item.puntuacion = item.likes - item.dislikes
                item.save()
                oldvote.voto = 1
                oldvote.save()
                item.alimentador.puntuacion += 2
                item.alimentador.save()

        except:
            if vote == "like":
                item.likes += 1
                item.puntuacion = item.likes - item.dislikes
                item.save()
                newvote = Voto(user = user, item = item, voto = 1)
                newvote.save()
                item.alimentador.puntuacion += 1
                item.alimentador.save()
            else:
                item.dislikes += 1
                item.puntuacion = item.likes - item.dislikes
                item.save()
                newvote = Voto(user = user, item = item, voto = -1)
                newvote.save()
                item.alimentador.puntuacion -= 1
                item.alimentador.save()

    if page == 'index':
        url = '/'
    else:
        if "Revision history" in feeder:
            url = "/alimentadores/" + str(item.alimentador) + "/" + id
        else:
            url = "/alimentadores/" + str(item.alimentador) + "/" + id
    
    return redirect(url)

def informacion(request):

    alimentadores = Alimentador.objects.all()
    context = {"alimentadores":alimentadores}

    user = request.user
    if user.is_authenticated:
        opciones = Opciones.objects.get(user=user)
        imagenusuario = Imagen.objects.get(user=user)
        context = {"opciones":opciones, "alimentadores":alimentadores,"imagenusuario":imagenusuario}
    return(render(request, "informacion.html", context))

def loggedIn(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
    else:
        user = request.user
    if user is not None and user.is_active:
        login(request, user)
        logged = "Logged in as " + user.username + " <a href='/'>back</a>"
        return redirect("/")
    else:
        logged = "Wrong user or password. <a href='/admin/'>Login via admin</a>"
    return HttpResponse(logged)

def logoutView(request):
    logout(request)
    return redirect("/") 

def register(request):
    form = UserCreationForm()
    if request.method == "POST":
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            imagen = Imagen(user=user)
            imagen.save()
            opciones = Opciones(user=user)
            opciones.save()
            if user is not None:
                login(request, user)
                return redirect("/usuarios/" + user.username)

    return render(request, "registro.html", {"form": form})