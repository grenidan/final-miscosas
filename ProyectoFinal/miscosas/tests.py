from django.test import TestCase
from django.db.models import QuerySet
from django.template import Template

from . import views
from .models import Alimentador
from django.contrib.auth.models import User

class TestAlimentador(TestCase):

    def setUp(self):
        self.alimentadoryt = 'Date un Vlog'
        self.id = 'UCQX_MZRCaluNKxkywkLEgfA'

    def test_crear_alimentador(self):
        alimentador = Alimentador(titulo=self.alimentadoryt, id=self.id)
        alimentador.save()


class TestUser(TestCase):

    def setUp(self):
        self.usuario = User(email='daniel@gmail.com', username="Daniel")
        self.usuario.save()
        self.credenciales = {
            'username': self.usuario,
            'password': 'contraseña'
        }

    def test_user_to_string_name(self):
        self.assertEquals(self.usuario.username, "Daniel")

    def test_logout_login_get(self):

        response = self.client.post("/logout")
        self.assertEqual(response.status_code, 302)

        response = self.client.post("/login", self.credenciales)
        self.assertEqual(response.status_code, 200)

        response = self.client.post("/loggedIn", self.credenciales)
        self.assertEqual(response.status_code, 200)

        response = self.client.post("/logout")
        self.assertEqual(response.status_code, 302)

class TestViews(TestCase):

    def setUp(self):
        self.alimentadorwiki = 'Leganes - Revision history'
        alimentadorwiki = Alimentador(titulo=self.alimentadorwiki, elegido = True)
        alimentadorwiki.save()
        self.alimentadoryt = 'Date un Vlog'
        self.item = '2RB6arKZnos'
        self.id = 'UCQX_MZRCaluNKxkywkLEgfA'
        self.usuario = 'admin'

    def test_get_barra(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        context = response.context
        self.assertIsInstance(context['alimentadores'], QuerySet)

        response = self.client.get('/?format=xml')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/?format=json')
        self.assertEqual(response.status_code, 200)

    def test_main_get_post_barra(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/alimentadores')
        self.assertEqual(response.status_code, 200)

        response = self.client.post('/alimentadores/' + self.alimentadorwiki, {'id': self.alimentadorwiki, 'Select':'Elegir'})
        self.assertEqual(response.status_code, 200)

    def test_index_content(self):
        checks = ["<h2>Items con mejores puntuaciones</h2>"]
        response = self.client.get('/')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)   

    def test_index_view(self):
        response = self.client.get('/')
        self.assertEqual(response.resolver_match.func, views.index)

    def test_get_alimentadores(self):
        response = self.client.get('/alimentadores')
        self.assertEqual(response.status_code, 200)

    def test_main_get_post_alimentadores(self):

        response = self.client.get('/alimentadores')
        self.assertEqual(response.status_code, 200)

        response = self.client.post('/alimentadores/' + self.alimentadorwiki, {'id': self.alimentadorwiki, 'Select':'Elegir'})
        self.assertEqual(response.status_code, 200)

    def test_alimentadores_view(self):
        response = self.client.get('/alimentadores')
        self.assertEqual(response.resolver_match.func, views.alimentadores)

    def test_get_alimentador(self):
        response = self.client.get('/alimentadores', {'id':self.alimentadoryt})
        self.assertEqual(response.status_code, 200)

    def test_alimentadores_content(self):
        checks = ["<h1>Alimentadores de Youtube</h1>",
                    "<h1>Alimentadores de Wikipedia</h1>"]
        response = self.client.get('/alimentadores')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content) 

    def test_get_item(self):
        response = self.client.get('/alimentadores', {'id':self.alimentadoryt})
        self.assertEqual(response.status_code, 200) 

    def test_get_usuarios(self):
        response = self.client.get('/usuarios')
        self.assertEqual(response.status_code, 200)

    def test_usuarios_view(self):
        response = self.client.get('/usuarios')
        self.assertEqual(response.resolver_match.func, views.usuarios)

    def test_usuarios_content(self):
        checks = ["<h2>Usuarios registrados</h2>"]
        response = self.client.get('/usuarios')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content) 

    def test_get_usuario(self):
        response = self.client.get('/usuarios/' + self.usuario)
        self.assertEqual(response.status_code, 200)

    def test_usuario_view(self):
        response = self.client.get('/usuarios/' + self.usuario)
        self.assertEqual(response.resolver_match.func, views.usuario)

    def test_get_informacion(self):
        response = self.client.get('/informacion')
        self.assertEqual(response.status_code, 200)
        context = response.context
        self.assertIsInstance(context['alimentadores'], QuerySet)

    def test_informacion_view(self):
        response = self.client.get('/informacion')
        self.assertEqual(response.resolver_match.func, views.informacion)

    def test_404(self):
        response = self.client.get('/noexiste/')
        self.assertEqual(response.status_code, 404)
