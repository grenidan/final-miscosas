from django.contrib import admin
from .models import Alimentador, Item, Voto, Comentario, Imagen, Opciones

admin.site.register(Alimentador)
admin.site.register(Item)
admin.site.register(Voto)
admin.site.register(Comentario)
admin.site.register(Imagen)
admin.site.register(Opciones)
