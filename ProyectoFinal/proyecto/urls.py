from django.contrib import admin
from django.urls import include, path
from django.contrib.auth.views import LoginView as login
from miscosas import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', include('miscosas.urls')),
    path('admin', admin.site.urls),
    path('login', login.as_view()),
    path('register', views.register),
] 

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
