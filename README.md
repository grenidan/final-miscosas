# final-miscosas

# Entrega practica
## Datos
* Nombre: Daniel Rodríguez Población
* Titulación: Tecnologías de la Telecomunicación
* Despliegue (url): http://grenidan.pythonanywhere.com/
* Video básico (url): https://www.youtube.com/watch?v=VcZPIdHRGxc
* Video parte opcional (url): https://www.youtube.com/watch?v=nJbRMqN6Ivk
## Cuenta Admin Site
* usuario: admin
* contraseña: admin
## Cuentas usuarios
* usuario: grex
* contraseña: paquitos
* usuario: daniel
* contraseña: tartadechocolate
## Resumen parte obligatoria
* Parte obligatoria funcional, con descarga de contenidos, usuarios, comentarios, votos, imágenes y demás funcionalidades requeridas en el enunciado
## Lista partes opcionales
* Implementación de un favicon
* Traducción al inglés mediante i18n, a través de un formulario para elegir el idioma
* Utilización de Bootstrap para la maquetación de toda la web
* COmentarios con imágenes subiendo una imgen a la aplicación y mostrándose posteriormente